package main

import (
	"fmt"
	"math/rand"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/labstack/gommon/log"

	"github.com/BurntSushi/toml"

	"github.com/sclevine/agouti"
)

const (
	BASEURL = "https://suzuri.jp"
)

func init() {
	// setup Logger
	f, err := os.OpenFile("server.log", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		panic(err)
	}
	log.SetOutput(f)
}

func main() {
	c := loadConfig("config.toml")
	fmt.Printf("%+v\n", c)

	for i := 0; i < c.N; i++ {
		fmt.Println(i+1, "回目のブラウザ実行")
		if err := c.autoFav(); err != nil {
			log.Error(err)
		}

		time.Sleep(time.Second)
	}
}

func (p *Config) autoFav() error {
	// ChromeDriverを起動
	var height int = 2436
	driver := agouti.ChromeDriver(
		agouti.ChromeOptions("args", []string{
			"--headless", // headlessモードの指定
			"--disable-gpu",
			fmt.Sprintf("--window-size=1125,%d", height), // ウィンドウサイズの指定
		}),
		agouti.Debug,
	)
	if err := driver.Start(); err != nil {
		log.Fatal(err)
	}
	defer driver.Stop()

	page, err := driver.NewPage()
	if err != nil {
		log.Fatal(err)
	}

	if err := page.Navigate(path.Join(BASEURL, p.FavoAccount)); err != nil {
		return err
	}

	var (
		arg = map[string]interface{}{
			"res": "",
		}
		result  interface{}
		heights int

		load = randInt(0, 10)
	)
	for i := 0; i < load; i++ {
		// 指定ブラウザの高さ分scrollし商品を最後まで取得する
		heights += height
		if err := page.RunScript(fmt.Sprintf("scroll(0, %d);", heights), arg, &result); err != nil {
			fmt.Println(err)
			break
		}
		time.Sleep(time.Second)
	}

	n, err := page.AllByClass("js-favorite-button").Count()
	if err != nil {
		return err
	}

	fmt.Printf("ブラウザアクセス時初期読み込みで%d個商品を検出\n", n)

	elements, err := page.AllByClass("js-favorite-button").Elements()
	if err != nil {
		return err
	}

	rand.Seed(time.Now().UnixNano())
	for i, v := range elements {
		num := rand.Int()
		// 乱数が７で割り切れればクリック
		if num%7 == 0 {
			times, _ := v.GetText()
			fmt.Printf("%d商品目の%sFAVOを+1\n", i, times)
			if err := v.Click(); err != nil {
				fmt.Println("does not click on fav")
			}
			time.Sleep(time.Second)
		}
	}

	return nil
}

type Config struct {
	FavoAccount string
	N           int //0 < NでランダムFavo
}

func loadConfig(filename string) Config {
	abs, err := filepath.Abs("./")
	if err != nil {
		log.Fatal(err)
	}

	pathname := filepath.Join(abs, filename)

	var config Config
	if _, err := toml.DecodeFile(pathname, &config); err != nil {
		log.Fatal(err)
	}

	return config
}

func randInt(start, end int) int {
	rand.Seed(time.Now().UnixNano())

	return rand.Intn(end-start) + start
}
